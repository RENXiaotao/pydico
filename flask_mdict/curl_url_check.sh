#!/bin/bash
count=0
declare -a urls=(
  "http://www10.unine.ch/sun/"
  "https://portal.unine.ch/secure/loginsun.php"
  "http://www10.unine.ch/sun/types/disciplines/"
  "http://www10.unine.ch/sun/inscriptions-onlinei/"
)
while :; do
  echo "Press 'q' to exit"
  read -t 3 -n 1 k <&1
  if [[ $k = q ]]; then
    break
  else
    for site in "${urls[@]}"; do

      if curl -I --connect-timeout 20 "$site" 2>&1 | grep -w "200\|301"; then
        echo "$site is up"
      else
        echo "$site is down"
      fi
      echo "----------------------------------"
    done
    count=$((count + 1))
    printf "\nIterate for %d times\n" "$count"
  fi
done
