import sqlite3 as db
import pandas as pd

# conn = db.connect('author_book_publisher.db')
conn = db.connect('../mdict/ecdict_wfd.db')

# conn.execute('''CREATE TABLE COMPANY
#          (ID INT PRIMARY KEY     NOT NULL,
#          NAME           TEXT    NOT NULL,
#          AGE            INT     NOT NULL,
#          ADDRESS        CHAR(50),
#          SALARY         REAL);''')
# conn.execute("INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) \
#       VALUES (1, 'Paul', 32, 'California', 20000.00 )");
#
# conn.execute("INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) \
#       VALUES (2, 'Allen', 25, 'Texas', 15000.00 )");
#
# conn.execute("INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) \
#       VALUES (3, 'Teddy', 23, 'Norway', 20000.00 )");
#
# conn.execute("INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) \
#       VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 )");
# conn.commit()

#cursor = conn.execute("SELECT * from ecdict")
cursor = conn.cursor()
cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
tables = cursor.fetchall()
print(tables)
for table_name in tables:
    table_name = table_name[0]
    print(table_name)
    table = pd.read_sql_query("SELECT * from %s where tag like 'gk'" % table_name, conn)
    table.to_csv(table_name + '.csv', index_label='index')
    print(table)
cursor.close()
conn.close()
#[print(row) for row in cursor]
#conn.close()