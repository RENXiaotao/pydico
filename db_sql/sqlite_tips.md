`sqlite> .read create.sql`

In general, the SQLite project has really fantastic documentation! 
I know we often reach for Google before the docs, 
but in SQLite's case, the docs really are technical writing at its best. 
It's clean, clear, and concise.

SQLAlchemy is a powerful database access tool kit for Python, 
with its object-relational mapper (ORM) being one of its most famous components, 
and the one discussed and used here.

The ORM provided by SQLAlchemy sits between the SQLite database 
and your Python program and transforms the data flow between the database engine and Python objects. 
SQLAlchemy allows you to think in terms of objects and still retain the powerful features of a database engine.


`%s/^.\{0,2\}//` remove the first 2 characters from every line.

`5,8s/^.\{0,2\}//` remove the first 2 characters from line 5-8.