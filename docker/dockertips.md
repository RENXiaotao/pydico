
```sh
docker run -it --name dico  -p 8080:8080 -w /home/site -v C:\Users\xren\Dropbox\Python\pydico:/home/site/shared myimage

docker start  dico
docker exec -it dico bash
```

```sh
sudo apt update
sudo apt upgrade
sudo apt install zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```
```sh
apt install vim
```

```sh
pip install flask gunicorn
```

```sh
 gunicorn --bind 0.0.0.0:8080 wsgi:app
```

in PyCharm, 
Tools->Deployment->Automatic Upload

`%s/^.\{0,2\}//` remove the first 2 characters from every line.

`5,8s/^.\{0,2\}//` remove the first 2 characters from line 5-8.


- The site is served only over HTTP. Without enabling HTTPS, your site is fundamentally insecure if you want to transmit any sensitive data from client to server or vice versa. Using HTTP means that requests and responses are sent in plain text. You’ll fix that soon.

- The URL uses the non-standard port 8000 versus the standard default HTTP port number 80. It’s unconventional and a bit of an eyesore, but you can’t use 80 yet. That’s because port 80 is privileged, and a non-root user can’t—and shouldn’t—bind to it. Later on, you’ll introduce a tool into the mix that allows your app to be available on port 80.

Gunicorn is first and foremost a Python WSGI app server, and a battle-tested one at that:

- It’s fast, optimized, and designed for production.
- It gives you more fine-grained control over the application server itself.
- It has more complete and configurable logging.
- It’s well-tested, specifically for its functionality as an application server.

The cool thing about a Gunicorn config file is that it just needs to be valid Python code, with variable names corresponding to arguments.

There’s one more player to add to the request chain: a web server like Nginx.

Hold up—you’ve already added Gunicorn! Why do you need to add something new into the picture? The reason for this is that Nginx and Gunicorn are two different things, and they coexist and work as a team.

Firstly, Nginx is a web server in that it can serve files to a web user or client. Files are literal documents: HTML, CSS, PNG, PDF—you name it.

I count the points of connecting trains for the board game, Ticket to Ride.  At first, it is better to count the points straightforward.

Steps:
1. Read a ref and a game photo
2. Find game board and transform 
3. Find cities and rails on ref
4. Find cities and rails on game board
5. Read legends
6. Count rails for each color and calculate points
7. Find longest rail and grade



- [x] Interactively get board
- [x] Perspectively transform to a rect
- [ ] Detect cities and rails in ref
- [ ] Detect rails in game
- [ ] Detect legends
- [ ] Detect rails built
- [ ] Calculate and mark the points
- [ ] Find the longest rail
- [ ] Automatically get board
- [ ] NN understands the train
- [ ] NN understands the board

Optional
- [ ] Magnifying glass over cursor hover for interactive board corner selection
- [ ] Tip a rail to highlight scoreboard
- [ ] Redraw cities and rails

nginx logs
- access_log /var/log/nginx/access.log; 
- error_log /var/log/nginx/error.log;

gunicorn logs
- error_log /var/log/gunicorn/error.log;

supervisor logs
- sudo less /var/log/supervisor/supervisord.log
